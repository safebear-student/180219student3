package com.safebear.auto.tests;

import com.safebear.auto.utils.Utils;
import org.testng.Assert;
import org.testng.annotations.Test;

public class LoginTests extends BaseTest {

@Test
    public void loginTest() {
    // step 1 ACTION Open Web Application in Browser
    driver.get(Utils.getUrl());
    // Get Page Title
    Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "The Login Page didn't open, or the title text has changed");



    // step 1.1 Locate input fields


    // step 1.2 locate buttons

    // Step 2 ; enter login details
    loginPage.enterUsername("tester");
    loginPage.enterPassword("letmein");


    // Step 3 Find Login Button

    // Click Login button
    loginPage.clickLoginButton();

    // Get Page Title`
    Assert.assertEquals(toolsPage.getPageTitle(),"Tools Page", "The login page didn't open or the title page has changed");
    }
@Test
    public void failedLogintest(){
        // step 1 ACTION Open Web Application in Browser

        driver.get(Utils.getUrl());
        // Get Page Title
        Assert.assertEquals(loginPage.getPageTitle(), "Login Page", "The Login Page didn't open, or the title text has changed");



        // step 1.1 Locate input fields


        // step 1.2 locate buttons

        // Step 2 ; enter login details
        loginPage.enterUsername("tester1");
        loginPage.enterPassword("letmeout");


        // Step 3 Find Login Button

        // Click Login button
        loginPage.clickLoginButton();

        // Get Page Title`
        Assert.assertEquals(toolsPage.getPageTitle(),"Login Page", "The login page didn't open or the title page has changed");

    }




}
