package com.safebear.auto.pages.locators;

import lombok.Data;
import org.openqa.selenium.By;
@Data
public class LoginPageLocators {
    // locate by id
    // id = Username
    private By usernameLocatorId = By.id("username");
    //id = password
    private By passwordLocatorId = By.id("password");
    //id = "enter"
    private By loginButtonLocatorId = By.id("enter");
}
