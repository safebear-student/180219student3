package com.safebear.auto.utils;
// Define URL
// Define BROWSER

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

public class  Utils {

        private static final String URL = System.getProperty("url","http://localhost:8080");
        private static final String BROWSER = System.getProperty("browser","chrome");

        public static String getUrl() {
                return URL;
        }

        public static WebDriver getDriver() {

                System.setProperty("webdriver.chrome.driver","src/test/test resources/drivers/chromedriver 72.0.3626.109.exe");
                //System.setProperty("webdriver.chrome.driver","src/test/test resources/drivers/chromedriver 73.0.3683.20.exe");

                ChromeOptions options = new ChromeOptions();
                options.addArguments("window-size=1366,768");

                switch (BROWSER){
                        case "chrome":
                                return new ChromeDriver(options);

                        case "headless":
                                options.addArguments("headless","disable-gpu");
                                return new ChromeDriver(options);

                        default:
                                return new ChromeDriver(options);

                }



        }


}
